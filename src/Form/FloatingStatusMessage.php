<?php

namespace Drupal\floating_status_message\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Custom form.
 */
class FloatingStatusMessage extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'floating_status_message_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'floating_status_message.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('floating_status_message.settings');
        $form['floating_enable'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enable'),
            '#default_value' => $config->get('floating_enable'),
        ];
        $form['floating_position'] = array(
            '#type' => 'radios',
            '#title' => t('Position'),
            '#description' => t('Select a Position for Floating Status Message.'),
            '#options' => array(
                t('Left'),
                t('Right')),
            '#default_value' => $config->get('floating_position'));

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('floating_status_message.settings')
            ->set('floating_enable', $values['floating_enable'])
            ->set('floating_position', $values['floating_position'])
            ->save();
        parent::submitForm($form, $form_state);
    }

}